package booklist;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * 使用jrebel插件后还是不能对boot的jar进行热部署
 * 外部Tomcat的debug开启是没问题的
 */
@Controller
@RequestMapping("/readingList")
public class ReadingListController {

  private static final String reader = "craig";
  private Logger log = LoggerFactory.getLogger(ReadingListController.class);
  
	private ReadingListRepository readingListRepository;

	//采用构造器的方式来自动注入
	@Autowired
	public ReadingListController(ReadingListRepository readingListRepository) {
		this.readingListRepository = readingListRepository;
	}
	
	@RequestMapping(value = "/readingList",method=RequestMethod.GET)
	public String readersBooks(Model model) {
		
		List<Book> readingList = readingListRepository.findByReader(reader);
		if (readingList != null) {
			model.addAttribute("books", readingList);
		}
		System.out.println("532222222222");
		log.info("#$#$#");
		return "readingList";
	}
	
	@RequestMapping(value="/readingList",method=RequestMethod.POST)
	public String addToReadingList(Book book) {
		book.setReader(reader);
		readingListRepository.save(book);
		return "redirect:/readingList";
	}

}
