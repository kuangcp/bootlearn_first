package booklist;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Myth on 2017/2/25 0025
 */
@Controller
@RequestMapping("/h")
public class TestRun {
    @ResponseBody
    @RequestMapping("/i")
    public String j(){
        return "10290 90 89";
    }
}
