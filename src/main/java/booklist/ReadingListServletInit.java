package booklist;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Created by Myth on 2017/2/25 0025
 * 当要发布为war来使用就要添加这个类，并指定boot起点类的位置就可以发布到Tomcat运行了
 */
public class ReadingListServletInit extends SpringBootServletInitializer {
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder){
        return builder.sources(DemoApplication.class);
    }
}
